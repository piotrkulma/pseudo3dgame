package pl.piotrkulma.pseudo3d.map;

/**
 * Created by Piotr Kulma on 25.07.16.
 */
public final class Element {
    private boolean checked;
    private int x;
    private int y;
    private int type;
    private int distance;

    public Element(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.distance = 0;
        this.checked = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getType() {
        return type;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.checked = true;
        this.distance = distance;
    }
}
