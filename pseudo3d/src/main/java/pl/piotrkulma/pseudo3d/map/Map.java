package pl.piotrkulma.pseudo3d.map;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Piotr Kulma on 24.07.16.
 */
public final class Map {
    public static final int MAP_ELEMENT_EMPTY_SPACE = 0;
    public static final int MAP_ELEMENT_WALL        = 1;

    private int width;
    private int height;
    private int[] map;

    private Map(MapLoader loader) {
        this.width = loader.width;
        this.height = loader.height;
        this.map = loader.map;
    }

    public final static class MapLoader {
        private int width;
        private int height;
        private int[] map;

        public MapLoader() {
            this.map = null;
        }

        public Map loadMapFromResource(String path) throws IOException {
            int lineCounter = 0;
            int indexCounter = 0;
            String line;
            BufferedReader bufferedReader = getBufferedReaderFromResource(path);

            while((line = bufferedReader.readLine()) != null) {
                if(lineCounter == 0) {
                    this.width = Integer.parseInt(line);
                } else if(lineCounter == 1) {
                    this.height = Integer.parseInt(line);
                    map = new int[this.width * this.height];
                }  else {
                    for (int i = 0; i < line.length(); i++) {
                        this.map[indexCounter++] = line.charAt(i) - 48;
                    }
                }

                lineCounter++;
            }

            return new Map(this);
        }

        private BufferedReader getBufferedReaderFromResource(String path) throws FileNotFoundException {
            URL url = Map.class.getClassLoader().getResource(path);
            FileReader reader = new FileReader(url.getFile());
            BufferedReader bufferedReader = new BufferedReader(reader);

            return bufferedReader;
        }
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public int getMapValue(int x) {
        return this.map[x];
    }

    public int getElementsInMapCount(int type) {
        int counter = 0;
        for(int i=0; i<map.length; i++) {
            if(map[i] == type) {
                counter ++;
            }
         }

        return counter;
    }

    public int getMapLength() {
        return this.map.length;
    }
}
