package pl.piotrkulma.pseudo3d;

import pl.piotrkulma.pseudo3d.gui.GameFrame;

import java.awt.EventQueue;
import java.io.IOException;

/**
 * Created by Piotr Kulma on 24.07.16.
 */
public class Run {
    public static void main(String... args) {
        EventQueue.invokeLater(() -> {
            GameFrame frame = null;
            try {
                frame = new GameFrame();
            } catch (IOException e) {
                e.printStackTrace();
            }
            frame.setVisible(true);
        });
    }
}
