package pl.piotrkulma.pseudo3d.graphics;

/**
 * Created by Piotr Kulma on 30.07.16.
 */
public final class Point {
    private double x;
    private double y;

    public Point(Point point) {
        this.x = point.getX();
        this.y = point.getY();
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public int getIntX() {
        return (int)x;
    }

    public double getY() {
        return y;
    }

    public int getIntY() {
        return (int)y;
    }

}
