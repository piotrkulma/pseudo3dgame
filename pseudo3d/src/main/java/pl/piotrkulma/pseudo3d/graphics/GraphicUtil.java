package pl.piotrkulma.pseudo3d.graphics;

import pl.piotrkulma.pseudo3d.map.Element;
import pl.piotrkulma.pseudo3d.map.Map;

import java.awt.*;

import static pl.piotrkulma.pseudo3d.GameConfig.GAME_TILE_HALF_SIZE;
import static pl.piotrkulma.pseudo3d.GameConfig.GAME_TILE_SIZE;
import static pl.piotrkulma.pseudo3d.GameConfig.GRAPHICS_RAYS_COUNT;
import static pl.piotrkulma.pseudo3d.GameConfig.GRAPHICS_RAY_ANGLE_STEP;
import static pl.piotrkulma.pseudo3d.GameConfig.GRAPHICS_RAY_LENGTH;
import static pl.piotrkulma.pseudo3d.GameConfig.GRAPHICS_VISION_ANGLE;

/**
 * Created by Piotr Kulma on 24.07.16.
 */
public final class GraphicUtil {
    private static final double VALUE_180_D = 180d;

    private GraphicUtil() {
    }

    public static void drawRays(Graphics2D g2d, Element[] boxIndexArray, int surfWidth, int surfHeight, double angle, int px, int py) {
        for(int i=0; i<boxIndexArray.length; i++) {
            boxIndexArray[i].setChecked(false);
        }

        Point center = new Point(px * GAME_TILE_SIZE + GAME_TILE_HALF_SIZE, py * GAME_TILE_SIZE + GAME_TILE_HALF_SIZE);

        double tempAngle = GRAPHICS_VISION_ANGLE;

        double width = (surfWidth / GRAPHICS_RAYS_COUNT);

        for(int rayCounter=0; rayCounter<=GRAPHICS_RAYS_COUNT; rayCounter++) {
            for(int rayLength = 0; rayLength < GRAPHICS_RAY_LENGTH ; rayLength++) {
                Point p2 = rotate(new Point(center.getX() + rayLength, center.getY()), center, GraphicUtil.radians(angle + tempAngle));

                g2d.setColor(Color.BLUE);
                g2d.drawLine(
                        center.getIntX(),
                        center.getIntY(),
                        p2.getIntX(), p2.getIntY());

                double len = rayLength / 200;

                if (GraphicUtil.intersect(boxIndexArray, p2.getIntX(), p2.getIntY(), rayLength)) {
                    g2d.setColor(getColor(rayLength));
                    g2d.fillRect((int)(rayCounter * width),
                            rayLength,
                            (int)width,
                            surfHeight - rayLength*2);
                    break;
                }
            }
            tempAngle -= GRAPHICS_RAY_ANGLE_STEP;
        }
    }

    public static void drawMap(Map map, int px, int py, Graphics2D g2d) {
        int i = 0;
        int j = -1;
        for(int index=0; index<map.getMapLength(); index++) {
            if(index % map.getWidth() == 0) {
                i = 0;
                j++;
            }

            g2d.setColor(Color.BLACK);
            if(map.getMapValue(index) == Map.MAP_ELEMENT_WALL) {
                g2d.fillRect(i*GAME_TILE_SIZE, j*GAME_TILE_SIZE, GAME_TILE_SIZE, GAME_TILE_SIZE);
            }
            g2d.drawRect(i*GAME_TILE_SIZE, j*GAME_TILE_SIZE, GAME_TILE_SIZE, GAME_TILE_SIZE);

            i++;
        }

        g2d.setColor(Color.BLUE);
        g2d.fillRect(px * GAME_TILE_SIZE, py * GAME_TILE_SIZE, GAME_TILE_SIZE, GAME_TILE_SIZE);
    }

    public static Element[] getIndexArray(Map map, int type) {
        int counter = 0;
        Element[] indexArray = new Element[map.getElementsInMapCount(type)];

        int i = 0;
        int j = -1;

        for(int index=0; index<map.getMapLength(); index++) {
            if(index % map.getWidth() == 0) {
                i = 0;
                j++;
            }

            if(map.getMapValue(index) == type) {
                indexArray[counter++] = new Element(i, j, type);
            }
            i++;
        }

        return indexArray;
    }

    private static double radians(double degree) {
        return Math.PI * degree / VALUE_180_D;
    }

    private static Point rotate(Point p, Point c, double rad) {
        double x = (p.getX() - c.getX()) * Math.sin(rad) - (p.getY() - c.getY()) * Math.cos(rad) + c.getX();
        double y = (p.getX() - c.getX()) * Math.cos(rad) + (p.getY() - c.getY()) * Math.sin(rad) + c.getY();

        return new Point(x, y);
    }

    private static boolean intersect(Element[] elements, int rayX, int rayY, int distance) {
        for(Element e : elements) {
            if(isValueInside(rayX, e.getX()* GAME_TILE_SIZE, e.getX()* GAME_TILE_SIZE + GAME_TILE_SIZE)
                    && isValueInside(rayY, e.getY()* GAME_TILE_SIZE, e.getY()* GAME_TILE_SIZE + GAME_TILE_SIZE)) {
                if(e.isChecked() == false) {
                    e.setDistance(distance);
                }
                return true;
            }
        }

        return false;
    }

    private static Color getColor(int rayLength) {
        int color = 10;

        for(int i=50; i<200; i+=10) {
            if (rayLength >= i - 50 && rayLength < i) {
                color = 200 - i;
            }
        }

        return new Color(color, color, color);
    }

    private static boolean isValueInside(int val, int min, int max) {
        return (val >= min && val <=max);
    }
}
