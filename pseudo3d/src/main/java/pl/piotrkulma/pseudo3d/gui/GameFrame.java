package pl.piotrkulma.pseudo3d.gui;

import pl.piotrkulma.pseudo3d.map.Map;

import javax.swing.JFrame;

import java.io.IOException;

import static pl.piotrkulma.pseudo3d.GameConfig.GAME_WINDOW_HEIGHT;
import static pl.piotrkulma.pseudo3d.GameConfig.GAME_WINDOW_WIDTH;
import static pl.piotrkulma.pseudo3d.GameConfig.GAME_WINDOW_TITLE;

/**
 * Created by Piotr Kulma on 24.07.16.
 */
public class GameFrame extends JFrame {
    public GameFrame() throws IOException {
        init();
    }

    private void init() throws IOException {
        add(new Surface(new Map.MapLoader().loadMapFromResource("map/map01.txt")));

        setTitle(GAME_WINDOW_TITLE);
        setSize(GAME_WINDOW_WIDTH, GAME_WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
