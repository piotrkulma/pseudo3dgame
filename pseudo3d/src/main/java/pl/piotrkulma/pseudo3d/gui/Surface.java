package pl.piotrkulma.pseudo3d.gui;

import pl.piotrkulma.pseudo3d.map.Element;
import pl.piotrkulma.pseudo3d.map.Map;


import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static pl.piotrkulma.pseudo3d.graphics.GraphicUtil.drawMap;
import static pl.piotrkulma.pseudo3d.graphics.GraphicUtil.drawRays;
import static pl.piotrkulma.pseudo3d.graphics.GraphicUtil.getIndexArray;

/**
 * Created by Piotr Kulma on 24.07.16.
 */
public class Surface extends JPanel implements KeyListener {
    private int x = 25;
    private int y = 30;

    private double angle = 0;

    private Element[] boxIndexArray;
    private Map map;

    public Surface(Map map) {
        this.map = map;
        this.boxIndexArray = getIndexArray(this.map, Map.MAP_ELEMENT_WALL);

        this.setFocusable(true);
        this.addKeyListener(this);
    }

    private void draw(Graphics graphics) {
        Graphics2D g2d = (Graphics2D) graphics;

        //g2d.setColor(new Color(0, 100, 0));
        //g2d.fillRect(0, GAME_WINDOW_HEIGHT / 2, GAME_WINDOW_WIDTH, GAME_WINDOW_HEIGHT);

        drawRays(g2d, boxIndexArray, this.getWidth(), this.getHeight(), angle, x, y);
        drawMap(this.map, x, y, g2d);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        draw(g);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(angle >= 360d) {
            angle = 360d - angle;
        } else if(angle <= 0) {
            angle = 360d + angle;
        }

        if( e.getKeyCode() == KeyEvent.VK_UP) {
            if(angle >= 22.5d && angle < 67.5d) {
                x++;
                y++;
            } else if(angle >= 67.5d && angle < 112.5d) {
                x++;
            } else if(angle >= 112.5d && angle < 157.5d) {
                x++;
                y--;
            } else if(angle >= 157.5d && angle < 202.5) {
                y--;
            } else if(angle >= 202.5 && angle < 247.5) {
                x--;
                y--;
            } else if(angle >= 247.5 && angle < 292.5) {
                x--;
            } else if(angle >= 292.5 && angle < 337.5) {
                x--;
                y++;
            } else {
                y++;
            }
        } else if( e.getKeyCode() == KeyEvent.VK_DOWN) {
            //y++;
        } else if( e.getKeyCode() == KeyEvent.VK_RIGHT) {
            //x++;
        } else if( e.getKeyCode() == KeyEvent.VK_LEFT) {
            //x--;
        } else if( e.getKeyCode() == KeyEvent.VK_1) {
            angle += 1;
        } else if( e.getKeyCode() == KeyEvent.VK_2) {
            angle -= 1;
        }

        repaint();

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
