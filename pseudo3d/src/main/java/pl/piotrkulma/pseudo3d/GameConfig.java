package pl.piotrkulma.pseudo3d;

/**
 * Created by Piotr Kulma on 24.07.16.
 */
public class GameConfig {
    public static final double GRAPHICS_RAY_LENGTH = 1000d;
    public static final double GRAPHICS_RAYS_COUNT = 320d;
    public static final double GRAPHICS_VISION_ANGLE = 60d;
    public static final double GRAPHICS_RAY_ANGLE_STEP = GRAPHICS_VISION_ANGLE / GRAPHICS_RAYS_COUNT;

    public static final int GAME_TILE_SIZE      = 6;
    public static final int GAME_TILE_HALF_SIZE = GAME_TILE_SIZE / 2;

    public static final int GAME_WINDOW_WIDTH   = 640;
    public static final int GAME_WINDOW_HEIGHT  = 400;

    public static final String GAME_WINDOW_TITLE = "pseudo3d test";
}
